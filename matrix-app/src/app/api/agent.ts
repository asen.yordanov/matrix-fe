import { IPhoto } from "./../models/profile";
import { IUser, IUserFormValues } from "./../models/user";
import { history } from "./../../index";
/* eslint-disable import/no-anonymous-default-export */
import axios, { AxiosResponse } from "axios";
import { IActivitiesEnvelope, IActivity } from "../models/activity";
import { toast } from "react-toastify";
import { IProfile } from "../models/profile";

axios.defaults.baseURL = process.env.REACT_APP_API_URL;

axios.interceptors.request.use(
  (config) => {
    const token = window.localStorage.getItem("jwt");
    if (token) config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(undefined, (error) => {
  if (error.message === "Network Error" && !error.response) {
    toast.error("Network error - make sure API is running!");
  }
  const { status, data, config } = error.response;
  if (status === 404) {
    history.push("/notfound");
  }
  if (
    status === 400 &&
    config.method === "get" &&
    data.errors.hasOwnProperty("id")
  ) {
    history.push("/notfound");
  }
  if (status === 500) {
    toast.error("Server error - check the terminal for more info!");
  }
  throw error.response;
});

const repsonceBody = (response: AxiosResponse) => response.data;

const requests = {
  get: (url: string) => 
  axios.get(url)
  .then(repsonceBody),
  post: (url: string, body: {}) =>
    axios.post(url, body)
    .then(repsonceBody),
  put: (url: string, body: {}) =>
    axios.put(url, body)
    .then(repsonceBody),
  del: (url: string) => 
  axios.delete(url)
  .then(repsonceBody),
  postForm: (url: string, file: Blob) => {
    let formData = new FormData();
    formData.append("File", file);
    return axios
      .post(url, formData, {
        headers: { "Counter-type": "multipart/form-data" },
      })
      .then(repsonceBody);
  },
};

const Activities = {
  list: (params: URLSearchParams): Promise<IActivitiesEnvelope> =>
    axios
      .get("/activities", { params: params })
      .then(repsonceBody),
  details: (id: string) => requests.get(`/activities/${id}`),
  create: (activity: IActivity) => requests.post("/activities", activity),
  update: (activity: IActivity) =>
    requests.put(`/activities/${activity.id}`, activity),
  delete: (id: string) => requests.del(`/activities/${id}`),
  attend: (id: string) => requests.post(`/activities/${id}/attend`, {}),
  unattend: (id: string) => requests.del(`/activities/${id}/attend`),
};

const User = {
  current: (): Promise<IUser> => requests.get("/user"),
  login: (user: IUserFormValues): Promise<IUser> =>
    requests.post(`/user/login`, user),
  register: (user: IUserFormValues): Promise<IUser> =>
    requests.post(`/user/register`, user),
};

const Profiles = {
  get: (username: string): Promise<IProfile> =>
    requests.get(`/profile/${username}`),
  uploadPhoto: (photo: Blob): Promise<IPhoto> =>
    requests.postForm(`/photo`, photo),
  setMainPhoto: (id: string) => requests.post(`/photo/${id}/setMain`, {}),
  deletePhoto: (id: string) => requests.del(`/photo/${id}`),
  updateProfile: (profile: Partial<IProfile>) =>
    requests.put(`/profile`, profile),
  follow: (username: string) =>
    requests.post(`/profile/${username}/follow`, {}),
  unfollow: (username: string) => requests.del(`/profile/${username}/follow`),
  listFollowings: (username: string, predicate: string) =>
    requests.get(`/profile/${username}/follow?predicate=${predicate}`),
  listActivities: (username: string, predicate: string) =>
    requests.get(`/profile/${username}/activities?predicate=${predicate}`),
};

export default {
  Activities,
  User,
  Profiles,
};
